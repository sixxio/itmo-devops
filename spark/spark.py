import pyspark
from pyspark.sql import SparkSession

spark = SparkSession.builder.master("spark://spark-master:7077") \
                    .appName('Spark job example') \
                    .getOrCreate()
df = spark.createDataFrame([{'Date': '2023-10-16 09:35:00', 'Value': 177.925003, 'Volume': 334605},\
 {'Date': '2023-10-16 09:36:00', 'Value': 177.994995, 'Volume': 241602},\
 {'Date': '2023-10-16 09:37:00', 'Value': 178.070007, 'Volume': 265920},\
 {'Date': '2023-10-16 09:38:00', 'Value': 178.270004, 'Volume': 342377},\
 {'Date': '2023-10-16 09:39:00', 'Value': 177.979996, 'Volume': 304389},\
 {'Date': '2023-10-16 09:40:00', 'Value': 177.830002, 'Volume': 297904},\
 {'Date': '2023-10-16 09:41:00', 'Value': 178.027298, 'Volume': 257781},\
 {'Date': '2023-10-16 09:42:00', 'Value': 177.845993, 'Volume': 231763},\
 {'Date': '2023-10-16 09:43:00', 'Value': 177.789993, 'Volume': 204801},\
 {'Date': '2023-10-16 09:44:00', 'Value': 177.475006, 'Volume': 210362},\
 {'Date': '2023-10-16 09:45:00', 'Value': 177.270004, 'Volume': 228717}])
df.show()
df.agg({'value':'mean', 'volume':'mean'}).show()
spark.stop()