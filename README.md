При деплое разворачиваются Apache Airflow и Postgres.


Описание DAGa:
* С сайта NASDAQ забираются данные о стоимости акций компании Apple и помещаются в Postgres базу данных. (Airflow)
* Для датафрейма считаются средние по каждому параметру. (Spark)

Особенности:
* Установлен airflow code editor, позволяющий редактировать код DAGa из админки Airflow.
* Postgres доступен на 5433 порту хоста.

Требования: 
* Установлен docker, доступно интернет-соединение.

Деплой:
* Запускаем демон докера, если он не запущен.
```shell
sudo dockerd &
```
* Копируем репозиторий.
```shell
git clone https://github.com/airflow-plugins/dag_code_editor.git
```
* Поднимаем Airflow с помощью docker compose.
```shell
docker-compose up -d
```
* Airflow будет доступен по адресу http://localhost:8080, логин sixxio, пароль sixxio.
* Spark будет доступен по адресу http://localhost:4040.
* Для работы со Spark'ом необходимо добавить подключение в Airflow (тип Spark, хост spark://spark-master, порт 7077).

